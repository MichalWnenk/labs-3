﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3.Implementation
{
    public class WindaKosmiczna : IZasilanie, IPanel, ILina
    {

        public string Start()
        {
            return "Zasila!";
        }

        public string Stop()
        {
            return "Nie zasila!";
        }

        public string SprawdzStan()
        {
            return "Sprawny układ zasilajacy!";
        }

        public string Uruchom()
        {
            return "Panel uruchomiony!";
        }

        public string Gora()
        {
            return "Jazda do gory rozpoczeta!";
        }

        public string Dol()
        {
            return "Jazda w dol rozpoczeta!";
        }

        public string Naciaganie()
        {
            return "Naciagam line!";
        }

        public string Zwolnij()
        {
            return "Zwalniam line!";
        }

        public static IZasilanie funkcjaZasilanie(object component)
        {
            return component as IZasilanie;
        }

        public static IPanel funkcjaPanel(object component)
        {
            return component as IPanel;
        }

        public static ILina funkcjaLina(object component)
        {
            return component as ILina;
        }
    }
}
