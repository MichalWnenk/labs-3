﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab3.Contract
{
    public interface ILina
    {
        string Naciaganie();
        string Zwolnij();
    }
}
