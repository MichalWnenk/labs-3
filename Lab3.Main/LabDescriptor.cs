﻿using System;
using Lab3.Contract;
using Lab3.Implementation;
using Lab3.Main;


namespace Lab3
{
    public struct LabDescriptor
    {
        #region definitions

        public delegate object GetInstance(object component);
        
        #endregion

        #region P1

        public static Type I1 = typeof(IZasilanie);
        public static Type I2 = typeof(ILina);
        public static Type I3 = typeof(IPanel);

        public static Type Component = typeof(WindaKosmiczna);

        public static GetInstance GetInstanceOfI1 = WindaKosmiczna.funkcjaZasilanie;
        public static GetInstance GetInstanceOfI2 = WindaKosmiczna.funkcjaLina;
        public static GetInstance GetInstanceOfI3 = WindaKosmiczna.funkcjaPanel;
        
        #endregion

        #region P2

        public static Type Mixin = typeof(StartZasilanieMixin);
        public static Type MixinFor = typeof(IZasilanie);

        #endregion
    }
}
