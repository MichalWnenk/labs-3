﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab3.Contract;
using Lab3.Implementation;

namespace Lab3.Main
{
    public static class StartZasilanieMixin
    {
        public static string Zasilanie(this IZasilanie target)
        {
            return target.Start();
        }
    }
}
